package edu.lou;

public class Calculator21 {

	public Integer doBinaryOp(Integer f1, Integer f2, BinaryOperation<Integer> op) {
		return op.apply(f1, f2);
	}

	public static abstract class BinaryOperation<T> {
		public abstract T apply(T t1, T t2);
	}
}