package edu.lou;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.junit.Test;

public class ListTest {

	@Test
	public void removeAll() {
		List<String> list = new ArrayList<>();
		list.add("joe");
		list.add("biden");
		list.add("donald");
		list.add("trump");

		/*
		 * Neither 'for' nor 'foreach' on list (with list::remove) will do: Direct call
		 * to list::remove will result on inconsistent result or
		 * ConcurrentModificationException
		 */

		Iterator<String> iterator = list.iterator();
		while (iterator.hasNext()) {
			String element = iterator.next();
			if (element.length() % 2 != 0) {
				iterator.remove();
			}
		}
		assertEquals(1, list.size());

		iterator = list.iterator();
		while (iterator.hasNext()) {
			String element = iterator.next();
			if (element.length() % 2 == 0) {
				iterator.remove();
			}
		}
		assertEquals(0, list.size());
	}
}
