package edu.lou;

import org.junit.Test;

enum MaladieSousFormeDEnum {
	CANCER_BENIN, CANCER_TERMINAL
}

class MaladieSousFormeDeClasse {
	public static final String CANCER_BENIN = "CANCER_BENIN";
	public static final String CANCER_TERMINAL = "CANCER_TERMINAL";
}

public class EnumTest {

	@Test
	public void testEnum() {
		MaladieSousFormeDEnum maladie1 = MaladieSousFormeDEnum.CANCER_BENIN;
		maladie1 = MaladieSousFormeDEnum.CANCER_TERMINAL;
		// maladie1 = "Voiture"; // NON !
		
		String maladie2 = MaladieSousFormeDeClasse.CANCER_BENIN;
		maladie2 = MaladieSousFormeDeClasse.CANCER_TERMINAL;
		maladie2 = "Voiture";
	}
}
